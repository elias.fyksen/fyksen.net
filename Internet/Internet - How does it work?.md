# Internet - How does it work?

Some years ago i wanted to understand how the internet works, however it was not easy. My main resource was the internet, and google. If you have googled any complex topic before, you will quickly discover that its easy to find surface information, but to find the under the hood details, how things realy work, its much harder. Thats why im trying to make a series of tutorials that will show everything that happens from when you plug an ethernet cable into your machine and til you see the oh so familiar google screen in your browser. This series will also work if you use wifi, however i will not discuss the wifi protocol and how you connect to a wifi, scince its a to complex topic to start with. I will also mostly focus on IPv4, scince this is what most people have access to.

## Getting started

To get started we need some things. Firstly you will need a computer, with internet access. I also suggest installing a program called wireshark. Wireshark is available for linux, mac and windows. Wireshark allows you to see something called network packets. All network comunication done by your computer is done through these small packets of information, and seeing them in wireshark will help the learning process alot. You can get wireshark from https://www.wireshark.org/

## Overview of the course

The course is devided up in to small digestable bits of information. It will go trough something called the OSI-model.

### OSI-model

The OSI-model is a model consisting of layers used to abstract the workings of the network. This makes it a lot easyer to think about. The layers follow:

#### 7. Application layer
#### 6. Presentation layer
#### 5. Session layer
#### 4. Transport layer
#### 3. Network layer
#### 2. Data link layer
#### 1. Physical layer

We will start at the bottom of the model, at the physical layer, and work our way up to the top layer, the application layer.

### Wireshark

Once you have installed wireshark from https://www.wireshark.org/, we will need to double check that its working.
Open wireshark. When you have done this you should see a list of interfaces like this:


![alt](https://gitlab.com/elias.fyksen/fyksen.net/raw/master/Internet/wireshark.png?inline=false)


Click on the interface called **any**.
Then you should se a list of packets, like this:


![alt](https://gitlab.com/elias.fyksen/fyksen.net/raw/master/Internet/wireshark_capture.png?inline=false)


If you get an error like "The capture session could not be initiated on interface 'any' (You don't have permission to capture on that device).", then you need to run wireshark as administrator, google it if you dont know how. If you dont see any packets, you might not be connect to the internet, try visiting google.com. If you have any further problems, please comment them in the section below, and always remember google is your friend.

Hope ill see you in the next section, where we really start on the network, with the first layer, **the Physical Layer**.

[Next section](https://fyksen.net/404)